package com.example.demo.controller;

import com.example.demo.model.Instructor;
import com.example.demo.model.Program;
import com.example.demo.service.InstructorService;
import com.example.demo.service.ProgramService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/programs")
public class ProgramController {

    private ProgramService programService;
    private InstructorService instructorService;

    public ProgramController(ProgramService programService, InstructorService instructorService) {
        this.programService = programService;
        this.instructorService = instructorService;
    }

    @GetMapping("")
    public String home(){
        return "index";
    }

    @GetMapping("/list")
    public String getPrograms(Model theModel) {
        List<Program> programs = programService.allPrograms();
        List<Instructor> instructors = instructorService.allInstructors();
        theModel.addAttribute("instructors", instructors);
        theModel.addAttribute("programs", programs);
        return "programs";
    }

    @GetMapping("/add")
    public String showFormForAdd(Model theModel) {
        Program theProgram = new Program();
        //Instructor theInstructor = new Instructor();
        theModel.addAttribute("program", theProgram);
        //theModel.addAttribute("instructor", theInstructor);
        theModel.addAttribute("id", "0");
        return "addProgram";
    }

    @PostMapping("/save")
    public String addPrograms(@ModelAttribute("program") Program theProgram ){
        programService.saveProgram(theProgram);
        //instructorService
        return "redirect:/programs/list";
    }

    @GetMapping("/addinstructor")
    public String showFormForAddInstructor(Model theModel) {
        Instructor theInstructor = new Instructor();
        //Program theProgram = new Program();
        theModel.addAttribute("instructor", theInstructor);
        //theModel.addAttribute("program", theProgram);
        theModel.addAttribute("id", "0");
        return "addInstructor";
    }

    @PostMapping("/saveinstructor")
    public String addInstructors(@ModelAttribute("instructor") Instructor theInstructor ){
        instructorService.saveInstructor(theInstructor);
        return "redirect:/programs/list";
    }

}
