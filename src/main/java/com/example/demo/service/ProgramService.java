package com.example.demo.service;

import com.example.demo.model.Program;
import com.example.demo.repository.ProgramRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramService {

    private ProgramRepo programRepo;

    public ProgramService(ProgramRepo programRepo) {
        this.programRepo = programRepo;
    }

    public List<Program> allPrograms() {

        return programRepo.findAll();
    }

    public void saveProgram(Program theProgram) {
        programRepo.save(theProgram);
    }
}
