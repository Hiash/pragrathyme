package com.example.demo.service;

import com.example.demo.model.Instructor;
import com.example.demo.repository.InstructorRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstructorService {

    private InstructorRepo instructorRepo;

    public InstructorService(InstructorRepo instructorRepo) {
        this.instructorRepo = instructorRepo;
    }

    public List<Instructor> allInstructors() {
        return instructorRepo.findAll();
    }

    public void saveInstructor(Instructor theInstructor){
        instructorRepo.save(theInstructor);
    }
}
