package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

@Document(collection="program")
public class Program {

    @Id
    private String id;
    private String programName;
    @DBRef
    private Instructor instructor;

    public Program() {
    }

    public Program(String id, String programName, Instructor instructor) {
        this.id = id;
        this.programName = programName;
        this.instructor = instructor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public void setProgram(Instructor instructor){
        List<Program> programs = new ArrayList<>();
        instructor.setPrograms(programs);
    }

    @Override
    public String toString() {
        return "Program{" +
                "id=" + id +
                ", programName='" + programName + '\'' +
                ", instructor=" + instructor +
                '}';
    }
}
